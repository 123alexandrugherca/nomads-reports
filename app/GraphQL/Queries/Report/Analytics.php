<?php

namespace App\GraphQL\Queries\Report;

use App\FormAnalytics;
use App\Report;
use App\Submission;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\DB;
use MongoDB\Model\BSONArray;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Analytics
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return "Hello, {$args['name']}!";
    }

    public function totalReport($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $listFormId = $args['formIds'];

        $submissionCount = Submission::whereIn('formId', $listFormId)->get()->count();

        $viewCount = FormAnalytics::whereIn('formId', $listFormId)->get()->count();

        $submission = Submission::whereIn('formId', $listFormId);


        $browsers = Submission::raw()->aggregate(array(
            array(
                '$group' => array(
                    '_id' => '$browser',
                    'count' => array( '$sum' => 1 )
                )
            )
        ))->toArray();

        $browserList = [];

        foreach($browsers as $browser){
            $element = iterator_to_array($browser);
            $browserList[] = ['name' => $element['_id'], 'total' => $element['count']];
        }

        $countries = Submission::raw()->aggregate(array(
            array(
                '$group' => array(
                    '_id' => '$country',
                    'count' => array( '$sum' => 1 )
                )
            )
        ))->toArray();

        $countriesList = [];

        foreach($countries as $country){
            $element = iterator_to_array($country);
            $countriesList[] = ['name' => $element['_id'], 'total' => $element['count']];
        }




        return [
            'submissionCount' => $submissionCount,
            'viewCount' => $viewCount,
            'conversionRate' => $submissionCount / $viewCount,
            'timeSpentOnForm' => [
                'min' => $submission->min('timeSpentOnForm'),
                'max' => $submission->max('timeSpentOnForm'),
                'average' => $submission->avg('timeSpentOnForm'),
            ],
            'earning' => $submission->sum('amount'),
            'browserList' => $browserList,
            'countriesList' => $countriesList,
        ];
    }

    public function totalReportChart($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $listFormId = $args['formIds'];


        $choices = Submission::raw()->aggregate([
            [
                '$match' => [
                    'formId' => [
                        '$in' => $listFormId
                    ]
                ]
            ],
            [
                '$group' => array(
                    '_id' => '$fields.'.$args['controlId'],
                    'count' => array( '$sum' => 1 )
                )
            ]
        ])->toArray();

        $choicesList = [];

        foreach($choices as $choice){
            $element = iterator_to_array($choice);
            if(!empty($element['_id'])) {
                $choicesList[] = ['name' => $element['_id'], 'total' => $element['count']];
            }
        }

        return $choicesList;
    }

    public function lastSubmission($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $listFormId = $args['formIds'];

        $lastSubmissions = Submission::whereIn('formId', $listFormId)->take(50)->get()->toArray();

        $submissionList = [];

        foreach($lastSubmissions as $lastSubmission){

            $fields = [];
            foreach($lastSubmission['fields'] as $controlId => $controlValue){
                $fields[] = [
                    'id' => $controlId,
                    'value' => $controlValue
                ];
            }
                $submissionList[] = [
                    'formId' => $lastSubmission['formId'],
                    'fields' => $fields,
                    'createdAt' => $lastSubmission['created_at'],
                ];

        }

        return $submissionList;
    }

}
