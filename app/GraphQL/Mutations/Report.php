<?php

namespace App\GraphQL\Mutations;

use App\Chart;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Report as AppReport;

class Report
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function report($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if(!empty($args['id'])) {
            $report = AppReport::find($args['id']);
        }else{
            $report = new AppReport();
        }

        $report->name = $args['name'];

        if(!empty($args['formIds'])){
            $report->formIds = $args['formIds'];
        }

        if(!empty($args['description'])){
            $report->description = $args['description'];
        }

        $report->save();
        if(isset($args['charts'])) {
            foreach ($args['charts'] as $chartSingle) {
                if (isset($chartSingle['id']) && !empty($chartSingle['id'])) {
                    $chart = Chart::find($chartSingle['id']);
                } else {
                    $chart = new Chart();
                }
                $chart->name = $chartSingle['name'];
                if(!empty($chartSingle['categoryWidget'])){
                    $chart->categoryWidget = $chartSingle['categoryWidget'];
                }
                if(!empty($chartSingle['subCategoryWidget'])){
                    $chart->subCategoryWidget = $chartSingle['subCategoryWidget'];
                }
                if(!empty($chartSingle['controlIds'])){
                    $chart->controlIds = $chartSingle['controlIds'];
                }

                $report->charts()->save($chart);
            }
        }
        return $report;
    }

    public function chart($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if(!empty($args['id'])) {
            $chart = Chart::find($args['id']);
        }else{
            $chart = new Chart();
        }

        $chart->name = $args['name'];
        $chart->report_id = $args['reportId'];

        if(!empty($args['categoryWidget'])){
            $chart->categoryWidget = $args['categoryWidget'];
        }

        if(!empty($args['subCategoryWidget'])){
            $chart->subCategoryWidget = $args['subCategoryWidget'];
        }

        if(!empty($args['controlIds'])){
            $chart->controlIds = $args['controlIds'];
        }

        $chart->save();

        return $chart;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return mixed
     */
    public function delete($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $report = AppReport::find($args['id']);
        foreach($report->charts()->get() as $chartSingle){
            $chart = Chart::find($chartSingle->id);
            $chart->delete();
        }
        $report->delete();
        return $report;
    }
}
