<?php

namespace App\Http\Controllers;

use App\FormAnalytics;
use App\Submission;
use Illuminate\Http\Request;

class SubmissionController extends Controller
{
    const ERR_TIME_NOT_TRACKED = -1;

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param int $formID
     */
    public function submission($formID)
    {

        $submission = new Submission();

        $submission->formId = (int)$formID;
        $submission->fields = $this->request->has('fields')
            ? $this->request->input('fields')
            : [];
        $submission->country = $this->request->has('country')
            ? $this->request->input('country')
            : null;
        $submission->currency = $this->request->has('currency')
            ? $this->request->input('currency')
            : null;
        $submission->amount = $this->request->has('amount')
            ? (double) number_format($this->request->input('amount'),2)
            : 0;
        $submission->browser = $this->request->has('browser')
            ? $this->request->input('browser')
            : null;
        $submission->paymentProcessor = $this->request->has('paymentProcessor')
            ? (int) $this->request->input('paymentProcessor')
            : null;
        $submission->timeSpentOnForm = $this->request->has('timeSpentOnForm')
            ? (int) $this->request->input('timeSpentOnForm')
            : SubmissionController::ERR_TIME_NOT_TRACKED;

        $submission->submissionId = $this->request->has('submissionId')
            ? (string) $this->request->input('submissionId')
            : null;
        $submission->submissionNumericId = $this->request->has('submissionNumericId')
            ? (int) $this->request->input('submissionNumericId')
            : null;
        $submission->paymentComplete = $this->request->has('paymentComplete')
            ? (int) $this->request->input('paymentComplete')
            : null;

        $submission->save();
    }

    /**
     * @param int $formId
     * @return bool
     */
    public function markSubmissionAsPayed($formId){
        $submissionId = $this->request->has('submissionId')
            ? (string) $this->request->input('submissionId')
            :null;

        if(empty($submissionId)){
            return false;
        }

        $submission = Submission::where('submissionId', $submissionId)->first();
        if(!empty($submission)){
            $submission->paymentComplete = 1;
            $submission->save();
        }
    }

    public function trackViewForm($formId)
    {
        $ip = $this->request->has('ip')
            ? $this->request->input('ip')
            : null;
        $browser = $this->request->has('browser')
            ? $this->request->input('browser')
            : null;
        $country = $this->request->has('country')
            ? $this->request->input('country')
            : null;

        $formAnalytics = new FormAnalytics();
        $formAnalytics->formId = (int)$formId;
        $formAnalytics->ip = $ip;
        $formAnalytics->browser = $browser;
        $formAnalytics->country = $country;
        $formAnalytics->save();
    }

}
