<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Report extends Model
{

    protected $connection = 'mongodb';
    protected $collection = 'list';

    protected $fillable = [
        'name',
        'formIds'
    ];

    public function charts()
    {
        return $this->hasMany(Chart::class);
    }
}
