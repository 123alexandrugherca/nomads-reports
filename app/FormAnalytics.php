<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class FormAnalytics extends Model
{

    protected $connection = 'mongodb';
    protected $collection = 'formAnalytics';

    protected $fillable = [
        'formId',
        'ip',
        'country',
        'browser'
    ];
}
