<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Chart extends Model
{

    protected $connection = 'mongodb';
    protected $collection = 'chart';

    protected $fillable = [
        'name',
    ];

    public function report(){
        return $this->belongsTo(Report::class);
    }
}
