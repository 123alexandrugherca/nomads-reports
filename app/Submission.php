<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Submission extends Model
{

    protected $connection = 'mongodb';
    protected $collection = 'submissions';

    protected $fillable = [
        'formId',
        'fields',
        'country',
        'amount',
        'currency',
        'paymentProcessor',
        'browser',
        'timeSpentOnForm',
        'submissionId',
        'submissionNumericId',
        'paymentComplete'
    ];
}
