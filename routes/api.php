<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'cors'], function() {
    Route::post('/form/{formId}/submission', 'SubmissionController@submission')->where('formId', '[0-9]+');;
    Route::put('/form/{formId}/submission', 'SubmissionController@markSubmissionAsPayed')->where('formId', '[0-9]+');;
    Route::post('/form/{formId}/view', 'SubmissionController@trackViewForm')->where('formId', '[0-9]+');;
});
